open HolKernel Parse boolLib bossLib;

open concrltl2waaTheory concrwaa2gbaTheory mlstringTheory

val _ = new_theory "automataPrinting";

val _ = overload_on ("SPC", ``strlit " "``);
val _ = overload_on ("LP", ``strlit "("``);
val _ = overload_on ("RP", ``strlit ")"``);

val list_string_def = Define‘
  list_string f l = LP ^ concatWith SPC (MAP f l) ^ RP
’;

val quote_def = Define‘quote s = strlit "\"" ^ implode s ^ strlit "\""’;

val _ = overload_on ("sexp", ``list_string I``)
val _ = overload_on ("sexpsym", ``λs l. sexp (strlit s :: l)``)
val _ = overload_on ("numlist_string",
                     “list_string (toString o $&) : num list -> mlstring”)
val _ = overload_on ("strlist_string",
                     “list_string quote : string list -> mlstring”);

val ltlfrml_string_def = Define‘
  (ltlfrml_string (VAR (s:string)) = sexpsym "VAR" [quote s]) ∧
  (ltlfrml_string (N_VAR s) = sexpsym "N_VAR" [quote s]) ∧
  (ltlfrml_string (DISJ f1 f2) =
          sexpsym "DISJ" [ltlfrml_string f1; ltlfrml_string f2]) ∧
  (ltlfrml_string (CONJ f1 f2) =
          sexpsym "CONJ" [ltlfrml_string f1; ltlfrml_string f2]) ∧
  (ltlfrml_string (X f) =
          sexpsym "X" [ltlfrml_string f]) ∧
  (ltlfrml_string (U f1 f2) =
          sexpsym "U"  [ltlfrml_string f1; ltlfrml_string f2]) ∧
  (ltlfrml_string (R f1 f2) =
          sexpsym "R" [ltlfrml_string f1; ltlfrml_string f2])
’

val AAedge_string_def = Define‘
  AAedge_string (lab : string edgeLabelAA) =
    sexpsym "lab" [toString (&lab.edge_grp);
                   strlist_string lab.pos_lab;
                   strlist_string lab.neg_lab]
’;

val AAnode_string_def = Define‘
  AAnode_string n =
    sexpsym "node" [ltlfrml_string n.frml;
                    if n.is_final then strlit "T" else strlit "F";
                    list_string AAedge_string n.true_labels]
’;

val spt_string_def = Define‘
  spt_string (vf : α -> mlstring) (spt : α spt) : mlstring =
    sexpsym "spt" (foldi
                     (λk v acc. sexp [toString (&k); vf v] :: acc)
                     0
                     []
                     spt)
’;

val pair_string_def = Define‘
  pair_string f1 f2 p = sexp [f1 (FST p); f2 (SND p)]
’;

val graph_string_def = Define‘
  graph_string nf ef g =
    sexpsym "graph" [
      spt_string nf g.nodeInfo;
      spt_string (list_string (pair_string ef (toString o $&))) g.followers;
      spt_string (list_string (pair_string ef (toString o $&))) g.preds;
      toString (&g.next)
    ]
’;

val AAstring_def = Define‘
  AAstring (cAA : string concrAA) =
    sexp [graph_string AAnode_string AAedge_string cAA.graph;
          list_string numlist_string cAA.init;
          strlist_string cAA.atomicProp]
’;

val GBAnode_string_def = Define‘
  GBAnode_string (n:string nodeLabelGBA) = sexp (MAP ltlfrml_string n.frmls)
’;

val GBAedge_string_def = Define‘
  GBAedge_string (e : string edgeLabelGBA) =
    sexpsym "gbE" [strlist_string e.pos_lab;
                   strlist_string e.neg_lab;
                   list_string ltlfrml_string e.acc_set]
’;

val GBAstring_def = Define‘
  GBAstring gba =
    sexpsym "GBA" [graph_string GBAnode_string GBAedge_string gba.graph;
                   numlist_string gba.init;
                   list_string ltlfrml_string gba.all_acc_frmls;
                   strlist_string gba.atomicProp]
’;

val _ = export_theory();
