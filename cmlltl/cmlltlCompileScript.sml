open preamble compilationLib cmlltlProgTheory

val _ = new_theory "cmlltlCompile"

val ltl_compiled = save_thm("ltl_compiled",
  compile_x64 1000 1000 "main" ltl_prog_def);

val _ = export_theory ();
