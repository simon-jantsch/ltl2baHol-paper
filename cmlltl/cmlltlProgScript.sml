open preamble basis

local open concrltl2waaTheory cmlltlCoreTheory in end

val _ = new_theory "cmlltlProg";

val _ = translation_extends "cmlltlCore";

fun def_of_const tm = let
  val res = dest_thy_const tm handle HOL_ERR _ =>
              failwith ("Unable to translate: " ^ term_to_string tm)
  val name = (#Name res)
  fun def_from_thy thy name =
    DB.fetch thy (name ^ "_def") handle HOL_ERR _ =>
    DB.fetch thy (name ^ "_DEF") handle HOL_ERR _ =>
    DB.fetch thy (name ^ "_thm") handle HOL_ERR _ =>
    DB.fetch thy name
  val def = def_from_thy (#Thy res) name handle HOL_ERR _ =>
            failwith ("Unable to find definition of " ^ name)
  in def end

val _ = find_def_for_const := def_of_const;

val _ = (append_prog o process_topdecs) ‘
  fun main() =
    let
      val print =
          case CommandLine.arguments() of
              ["-n"] => false
            | _ => true
      val form_str = TextIO.inputAll TextIO.stdIn
    in
      case parse (String.explode form_str) of
          NONE => TextIO.print "Failed to parse input\n"
        | SOME f => (
            case expandauto_init (nnf f) of
                NONE => TextIO.print "Failed at LTL -> VWAA!\n"
              | SOME vwaa =>
                (if print then (TextIO.print "Resulting VWAA:\n";
                                TextIO.print (aastring vwaa ^ "\n"))
                 else TextIO.print "VWAA generated\n";
                 case expandgba_init vwaa of
                     NONE => TextIO.print "Failed at VWAA -> GBA!\n"
                   | SOME gba => if print then
                                   (TextIO.print "Resulting GBA:\n";
                                    TextIO.print (gbastring gba ^ "\n"))
                                 else TextIO.print "GBA generated\n")
          )
    end
’;

val st = get_ml_prog_state();

val main_sem_def = Define‘
  main_sem printp fs =
    let
      fopt =
        case get_file_content fs 0 of
            SOME (c, p) => ltlPEG$parse (DROP p c)
          | NONE => NONE ;
      outstr =
        case fopt of
            NONE => strlit "Failed to parse input\n"
          | SOME f => (
            case expandAuto_init (NNF f) of
                NONE => strlit "Failed at LTL -> VWAA!\n"
              | SOME vwaa =>
                (if printp then (strlit "Resulting VWAA:\n" ^
                                 AAstring vwaa ^ strlit "\n")
                 else strlit "VWAA generated\n") ^
                (case expandGBA_init vwaa of
                     NONE => strlit "Failed at VWAA -> GBA!\n"
                   | SOME gba => if printp then
                                   strlit "Resulting GBA:\n" ^
                                   GBAstring gba ^ strlit "\n"
                                 else strlit "GBA generated\n")
          )
    in
      add_stdout (fastForwardFD fs 0) outstr
’;

val sepimpGC_refl = Q.prove(
  ‘P ==>> P * GC’,
  xsimpl);

val ltl_spec = Q.store_thm("ltl_spec",
  ‘(∃out0. stdout fs out0) ∧ stdin fs input pos ⇒
   app (p:'ffi ffi_proj) ^(fetch_v "main" st) [Conv NONE []]
     (STDIO fs * COMMANDLINE cl)
     (POSTv uv. &UNIT_TYPE () uv * STDIO (main_sem (TL cl ≠[strlit "-n"]) fs) *
                COMMANDLINE cl)’,
  strip_tac >> simp[main_sem_def] >>
  xcf "main" st >> xmatch >>
  xlet_auto >- (xcon >> xsimpl) >> (* apply arguments to unit *)
  xlet_auto >- (CONV_TAC SWAP_EXISTS_CONV >> qexists_tac ‘cl’ >> xsimpl) >>
  xlet ‘POSTv bv. &BOOL (TL cl ≠ [strlit "-n"]) bv * COMMANDLINE cl * STDIO fs’
  >- (xmatch >>
      reverse conj_tac
      >- (EVAL_TAC >> Cases_on ‘TL cl’
          >- (fs[LIST_TYPE_def] >> EVAL_TAC >> simp[])
          >- (fs[LIST_TYPE_def] >> rename [‘STRING_TYPE s sv’] >>
              Cases_on ‘s’ >> fs[STRING_TYPE_def] >> EVAL_TAC >>
              rw[semanticPrimitivesTheory.lit_same_type_def] >>
              rename [‘LIST_TYPE STRING_TYPE ttl tlv’] >> Cases_on ‘ttl’ >>
              fs[LIST_TYPE_def] >> EVAL_TAC)) >>
      Cases_on ‘TL cl’ >> fs[LIST_TYPE_def]
      >- (conj_tac >- (xcon >> xsimpl) >> EVAL_TAC >> simp[]) >>
      rename [‘STRING_TYPE s sv’] >> Cases_on ‘s’ >> fs[STRING_TYPE_def] >>
      rw[]
      >- (xcon >> xsimpl >> rename [‘LIST_TYPE STRING_TYPE ttl _’] >>
          Cases_on ‘ttl’ >> fs[LIST_TYPE_def])
      >- (xcon >> xsimpl >> fs[] >> rename [‘LIST_TYPE STRING_TYPE ttl _’] >>
          Cases_on ‘ttl’ >> fs[LIST_TYPE_def])
      >- EVAL_TAC) >>
  drule_then assume_tac TextIOProofTheory.stdin_get_file_content >>
  xlet ‘POSTv form_s.
         &STRING_TYPE (implode (DROP pos input)) form_s *
         STDIO (fastForwardFD fs 0) * COMMANDLINE cl’
  >- (xapp >> goal_assum drule >> simp[TextIOProofTheory.FD_stdin] >>
      xsimpl) >>
  qabbrev_tac `fstr = DROP pos input` >>
  qabbrev_tac `fs1 = fastForwardFD fs 0` >>
  xlet_auto >- xsimpl >> (* fstr (explode input string) *)
  xlet_auto >- xsimpl >> rename [‘OPTION_TYPE _ (parse fstr) form_v’] >>
  xmatch >> xsimpl >>
  Cases_on ‘parse fstr’ >> fs[OPTION_TYPE_def]
  >- (reverse conj_tac >- (EVAL_TAC >> simp[] (* validate_pat *)) >>
      xapp >> xsimpl >> CONV_TAC SWAP_EXISTS_CONV >> qexists_tac ‘fs1’ >>
      xsimpl) >>
  reverse (rpt conj_tac) >- (EVAL_TAC >> simp[]) >- (EVAL_TAC >> simp[]) >>
  rename [‘parse fstr = SOME aa’] >>
  qabbrev_tac ‘printp = (TL cl ≠ [strlit "-n"])’ >>
  xlet_auto >- xsimpl >> rename [‘LTL_LTL_FRML_TYPE _ (NNF aa) nnfaa_v’] >>
  xlet_auto >- xsimpl >> rename [‘OPTION_TYPE _ (expandAuto_init _) vwaa_v’] >>
  xmatch >> xsimpl >>
  Cases_on ‘expandAuto_init (NNF aa)’ >> fs[OPTION_TYPE_def]
  >- (reverse conj_tac >- (EVAL_TAC >> simp[]) >> xapp >> xsimpl >>
      CONV_TAC SWAP_EXISTS_CONV >> qexists_tac ‘fs1’ >> xsimpl) >>
  reverse (rpt conj_tac) >- (EVAL_TAC >> simp[]) >- (EVAL_TAC >> simp[]) >>
  rename [‘expandAuto_init (NNF aa) = SOME vwaa’] >>
  xlet ‘POSTv uv. &UNIT_TYPE () uv * COMMANDLINE cl *
                  STDIO (add_stdout fs1 (if printp then
                                           strlit "Resulting VWAA:\n" ^
                                           AAstring vwaa ^ strlit "\n"
                                         else strlit "VWAA generated\n"))’
  >- (xif
      >- (xlet_auto >- xsimpl >> (* print "Resulting VWAA:\n" *)
          xlet_auto >- xsimpl >> (* make call to AAstring *)
          xlet_auto >- xsimpl >> (* concatenate in AAstring *)
          xapp >> xsimpl >> goal_assum (drule_then mp_tac) >>
          qexists_tac ‘COMMANDLINE cl’ >> xsimpl >>
          qexists_tac ‘add_stdout fs1 (strlit "Resulting VWAA:\n")’ >> xsimpl >>
          metis_tac[strcat_assoc, add_stdo_o, sepimpGC_refl, DECIDE ``1n ≠ 0``,
                    stdo_fastForwardFD]) >>
      xapp >> qexists_tac ‘COMMANDLINE cl’ >> xsimpl >>
      qexists_tac ‘fs1’ >> xsimpl) >>
  qabbrev_tac ‘
    pfx = if printp then
            strlit "Resulting VWAA:\n" ^ AAstring vwaa ^ strlit "\n"
          else strlit "VWAA generated\n"
  ’ >>
  xlet_auto >- xsimpl >>
  Cases_on ‘expandGBA_init vwaa’ >> fs[OPTION_TYPE_def] >> xmatch
  >- (xapp >> xsimpl >> qexists_tac ‘COMMANDLINE cl’ >> xsimpl >>
      qexists_tac ‘add_stdout fs1 pfx’ >> xsimpl >>
      metis_tac[strcat_assoc, add_stdo_o, sepimpGC_refl, DECIDE “1n ≠ 0”,
                stdo_fastForwardFD]) >>
  xif
  >- (xlet_auto >- (xsimpl >> simp[Abbr‘pfx’] >> xsimpl) >>
      xlet_auto >- (fs[] >> xsimpl) >> (* make call to GBAstring *)
      xlet_auto >- (fs[] >> xsimpl) >> (* concatenate strings *)
      fs[] >> xapp >> goal_assum (drule_then mp_tac) >>
      qexists_tac ‘COMMANDLINE cl’ >> xsimpl >>
      rename [‘expandGBA_init vwaa = SOME gba’] >>
      qexists_tac ‘
        add_stdout (add_stdout fs1 pfx) (strlit "Resulting GBA:\n")’ >>
      xsimpl >>
      metis_tac[strcat_assoc, add_stdo_o, sepimpGC_refl, DECIDE “1n ≠ 0”,
                stdo_fastForwardFD]) >>
  xapp >> xsimpl >> qexists_tac ‘COMMANDLINE cl’ >> xsimpl >>
  qexists_tac ‘add_stdout fs1 pfx’ >> xsimpl >>
  metis_tac[strcat_assoc, add_stdo_o, sepimpGC_refl, DECIDE “1n ≠ 0”,
            stdo_fastForwardFD]);

val ltl_whole_prog_spec = Q.store_thm("ltl_whole_prog_spec",
  `(∃out0. stdout fs out0) ∧ stdin fs input pos ⇒
   whole_prog_spec ^(fetch_v"main"st) cl fs
     ((=) (main_sem (TL cl ≠ [strlit "-n"]) fs))`,
  disch_then assume_tac >>
  simp[whole_prog_spec_def]
  \\ qexists_tac`main_sem (TL cl ≠ [strlit "-n"]) fs`
  \\ reverse conj_tac
  >- rw[main_sem_def,GSYM add_stdo_with_numchars,with_same_numchars,
        GSYM fastForwardFD_with_numchars]
  \\ match_mp_tac (MP_CANON (MATCH_MP app_wgframe (UNDISCH ltl_spec)))
  \\ xsimpl);

val (sem_thm,prog_tm) = whole_prog_thm st "main" (UNDISCH ltl_whole_prog_spec)
val ltl_prog_def = Define`ltl_prog = ^prog_tm`;

val _ = export_theory();
