open preamble basis

local open concrltl2waaTheory cmlparseFormTheory in end

val _ = new_theory "cmlltlCore";

val _ = translation_extends "cmlparseForm";

fun def_of_const tm = let
  val res = dest_thy_const tm handle HOL_ERR _ =>
              failwith ("Unable to translate: " ^ term_to_string tm)
  val name = (#Name res)
  fun def_from_thy thy name =
    DB.fetch thy (name ^ "_def") handle HOL_ERR _ =>
    DB.fetch thy (name ^ "_DEF") handle HOL_ERR _ =>
    DB.fetch thy (name ^ "_thm") handle HOL_ERR _ =>
    DB.fetch thy name
  val def = def_from_thy (#Thy res) name handle HOL_ERR _ =>
            failwith ("Unable to find definition of " ^ name)
  in def end

val _ = find_def_for_const := def_of_const;

(* translate printing functions *)
val _ = translate automataPrintingTheory.AAstring_def
val _ = translate automataPrintingTheory.GBAstring_def

val SUC = Q.prove(‘SUC = λn. n + 1’, simp[FUN_EQ_THM])
val addNode' = REWRITE_RULE [SUC] gfgTheory.addNode_def
val _ = translate addNode'

val IN_DOMAIN_def = Define‘
  (IN_DOMAIN x LN ⇔ F) ∧
  (IN_DOMAIN x (LS v) ⇔ (x = 0)) ∧
  (IN_DOMAIN x (BN b1 b2) ⇔
    if x = 0 then F
    else if EVEN x then IN_DOMAIN ((x - 2) DIV 2) b1
    else IN_DOMAIN ((x - 1) DIV 2) b2) ∧
  (IN_DOMAIN x (BS b1 v b2) ⇔
    if x = 0 then T
    else if EVEN x then IN_DOMAIN ((x - 2) DIV 2) b1
    else IN_DOMAIN ((x - 1) DIV 2) b2)
’;

val dumb = Q.prove(
  ‘((2 * n) DIV 2 = n) ∧ ((2 * n + 1) DIV 2 = n)’,
  conj_tac >- metis_tac[MULT_DIV, DECIDE “0 < 2n”, MULT_COMM] >>
  metis_tac[DIV_MULT, MULT_COMM, DECIDE “1 < 2n”]);

val IN_DOMAIN_thm = Q.prove(
  ‘∀spt x. x ∈ domain spt ⇔ IN_DOMAIN x spt’,
  Induct >> REWRITE_TAC [IN_domain] >> simp[IN_DOMAIN_def]
  >- (qx_gen_tac ‘x’ >> Cases_on ‘x = 0’ >> simp[] >>
      Cases_on ‘EVEN x’ >> simp[] >> fs[EVEN_EXISTS] >>
      rename [‘x = 2 * n’] >> Cases_on ‘n’ >>
      fs[ADD1, LEFT_ADD_DISTRIB, dumb])
  >- (qx_gen_tac ‘x’ >> Cases_on ‘x = 0’ >> simp[] >>
      Cases_on ‘EVEN x’ >> simp[] >> fs[EVEN_EXISTS] >>
      rename [‘x = 2 * n’] >> Cases_on ‘n’ >>
      fs[ADD1, LEFT_ADD_DISTRIB, dumb]));

val updateNode' = REWRITE_RULE [IN_DOMAIN_thm] gfgTheory.updateNode_def
val _ = translate updateNode'
val addEdge' = REWRITE_RULE [IN_DOMAIN_thm] gfgTheory.addEdge_def
val _ = translate addEdge'

val _ = translate concrltl2waaTheory.trans_concr_def;
val trans_concr_side = Q.prove(
  ‘∀x. trans_concr_side x ⇔ T’,
  Induct >> simp[Once (DB.fetch "-" "trans_concr_side_def")])
  |> update_precondition;

val _ = translate concrRepTheory.addEdgeToGraph_def;
val addedgetograph_side = Q.prove(
  ‘∀x1 x2 x3. addedgetograph_side x1 x2 x3 ⇔ T’,
  rpt gen_tac >> Cases_on ‘x2’ >> simp[DB.fetch "-" "addedgetograph_side_def"])
  |> update_precondition;

val _ = translate concrRepTheory.addFrmlToGraph_def;
val addfrmltograph_side = Q.prove(
  ‘∀x1 x2. addfrmltograph_side x1 x2 ⇔ T’,
  rpt gen_tac >> Cases_on ‘x2’ >> simp[DB.fetch "-" "addfrmltograph_side_def"])
  |> update_precondition;

val gref = ref ([] : term list, ``T``)
fun norecord w = (gref := w; NO_TAC w)

val _ = translate concrRepTheory.graphStates_def;
val egtranslated0 = translate_no_ind concrltl2waaTheory.expandGraph_def;

val ind_lemma = Q.prove(
  ‘^(first is_forall (hyp egtranslated0))’,
  rpt gen_tac
  \\ rpt (disch_then strip_assume_tac)
  \\ match_mp_tac (latest_ind ())
  \\ rpt strip_tac
  \\ last_x_assum match_mp_tac
  \\ rpt strip_tac
  \\ fs [FORALL_PROD] >>
  full_simp_tac (srw_ss() ++ ETA_ss) [GSYM CONJ_ASSOC])
 |> update_precondition;

val _ = translate concrltl2waaTheory.expandAuto_init_def;

(* VWAA -> GBA transformation *)
(* monad_bind over recursion seems to confuse the generation of the
   translator's symbolic evaluation of the body *)
val monad_bind_rwt = Q.prove(
  ‘monad_bind (x:'a option) f =
     case x of
       NONE => NONE
     | SOME y => f y’,
  Cases_on ‘x’ >> simp[]);
val _ = translate (concrwaa2gbaTheory.expandGBA_def
                     |> ONCE_REWRITE_RULE [monad_bind_rwt]
                     |> BETA_RULE)
val _ = translate concrwaa2gbaTheory.expandGBA_init_def

val _ = translate ltlTheory.NNF_def

val _ = export_theory();
