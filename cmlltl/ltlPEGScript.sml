open HolKernel Parse boolLib bossLib;

open ltlTheory

open pegTheory

local open formParseTheory in end

val _ = new_theory "ltlPEG";

val LTLT = zDefine‘LTLT = [LTL_TRUE ""]’;

val tokP_def = Define‘tokP P = tok P (K LTLT)’

val lws_def = Define`
  lws t = seq (rpt (tokP isSpace) (λl. LTLT)) t (λe1 e2. e2)
`;

val token_def = Define`
  token s = lws (FOLDR (λc p. seq (tokP ((=) c)) p (λt p. LTLT)) (empty LTLT) s)
`;

val iconcat_def = Define‘
  iconcat (a : string) [] = [F_VAR a] ∧
  iconcat a (F_VAR s :: rest) = iconcat (a ++ s) rest ∧
  iconcat a (_ :: rest) = []
’;

val identchar_def = Define‘
  identchar = tok (λc. isAlpha c ∧ isLower c) (λp. [F_VAR [FST p]])
’;

val ident_def = Define‘
  ident = seq identchar (rpt identchar (iconcat "" o FLAT))
              (λi1 i2. iconcat "" (i1 ++ i2))
’;

val _ = Datatype`ltlNT = nBase | nU | nConj | nDisj | nImp | nStart`

val lnt_def = Define`lnt (ntsym : ltlNT) = nt (INL ntsym) I`

val sumID_def = Define‘sumID (INL x) = x ∧ sumID (INR x) = x’;

val maybeBINOP0_def = Define‘
  (maybeBINOP0 f [] = []) ∧
  (maybeBINOP0 f [e] = [e]) ∧
  (maybeBINOP0 f (e1::e2::_) = [f e1 e2])
’;

val maybeBINOP_def = Define‘
  maybeBINOP f (l1 : 'a list) l2 = maybeBINOP0 f (l1 ++ l2)
’;

val choicel_def = Define`
  choicel [] = not (empty []) [] ∧
  choicel (h::t) = choice h (choicel t) sumID
`;

val HDf_def = Define‘(HDf f [] = []) ∧ (HDf f (h::t) = [f h])’

val LTL_FF_def = Define‘LTL_FF f = LTL_F f ""’;
val LTL_GG_def = Define‘LTL_GG f = LTL_G f ""’;

val ltlPEG_def = zDefine ‘
  ltlPEG = <|
    start := lnt nStart ;
    rules := FEMPTY |++ [
      (INL nImp, seq (lnt nDisj)
                     (choice (seq (token "->") (lnt nImp) (K I))
                             (empty [])
                             sumID)
                     (maybeBINOP F_IMP));
      (INL nDisj, seq (lnt nConj)
                      (choice (seq (token "|") (lnt nDisj) (K I))
                              (empty [])
                              sumID)
                      (maybeBINOP F_DISJ));
      (INL nConj, seq (lnt nU)
                      (choice (seq (token "&") (lnt nConj) (K I))
                              (empty [])
                              sumID)
                      (maybeBINOP F_CONJ));
      (INL nU, seq (lnt nBase)
                   (choice (seq (token "U") (lnt nU) (K I)) (empty []) sumID)
                   (maybeBINOP F_U));
      (INL nBase,
       choicel [
         seq (token "F") (lnt nBase) (K (HDf LTL_FF));
         seq (token "G") (lnt nBase) (K (HDf LTL_GG));
         seq (token "!") (lnt nBase) (K (HDf F_NEG));
         seq (token "X") (lnt nBase) (K (HDf F_X));
         seq (token "(") (seq (lnt nImp) (token ")") K) (K I);
         lws ident;
       ]);
      (INL nStart, seq (lnt nImp) (lws (empty [])) $++);
    ]
  |>
’;

val FDOM_ltlPEG = save_thm(
  "FDOM_ltlPEG",
  SIMP_CONV (srw_ss()) [ltlPEG_def,
                        finite_mapTheory.FRANGE_FUPDATE_DOMSUB,
                        finite_mapTheory.DOMSUB_FUPDATE_THM,
                        finite_mapTheory.FUPDATE_LIST_THM]
            ``FDOM ltlPEG.rules``);

val spec0 =
    pegexecTheory.peg_nt_thm
      |> Q.GEN `G`  |> Q.ISPEC `ltlPEG`
      |> SIMP_RULE (srw_ss()) [FDOM_ltlPEG]
      |> Q.GEN `n`

fun mk_rule_app n =
  SIMP_CONV (srw_ss())
            [ltlPEG_def, finite_mapTheory.FUPDATE_LIST_THM,
             finite_mapTheory.FAPPLY_FUPDATE_THM]
            (mk_comb(``FAPPLY ltlPEG.rules``, n))

val ltlNTs = [``INL nImp : ltlNT inf``,
              ``INL nU : ltlNT inf``,
              ``INL nDisj : ltlNT inf``,
              ``INL nConj : ltlNT inf``,
              ``INL nBase : ltlNT inf``,
              ``INL nStart : ltlNT inf``]

val ltlPEG_applied = save_thm(
  "ltlPEG_applied",
  LIST_CONJ (map mk_rule_app ltlNTs));

fun mkrule n = SIMP_RULE bool_ss [ltlPEG_applied] (SPEC n spec0)


val ltlpeg_exec_rules = save_thm(
  "ltlpeg_exec_rules",
  LIST_CONJ (map mkrule ltlNTs))

val _ = computeLib.add_persistent_funs ["ltlpeg_exec_rules"]

val loc0_def = Define`loc0 = Locs (locn 0 0 0) (locn 0 0 0)`;
val parse_def = Define‘
  parse s =
    case peg_exec ltlPEG (lnt nStart) (MAP (λc. (c, loc0)) s) [] done failed of
        Result (SOME ([], [e])) => SOME e
      | _ => NONE
’;

val wfpeg_rwts = wfpeg_cases
                   |> ISPEC ``ltlPEG``
                   |> (fn th => map (fn t => Q.SPEC t th)
                                    [`seq e1 e2 f`, `choice e1 e2 f`, `tok P f`,
                                     `any f`, `empty v`, `not e v`, `rpt e f`,
                                     `choicel []`, `choicel (h::t)`, `tokP t`
                      ])
                   |> map (CONV_RULE
                             (RAND_CONV (SIMP_CONV (srw_ss())
                                                   [choicel_def, tokP_def])))

val wfpeg_lnt = wfpeg_cases
                  |> ISPEC ``ltlPEG``
                  |> Q.SPEC `lnt n`
                  |> CONV_RULE (RAND_CONV (SIMP_CONV (srw_ss()) [lnt_def]))

val peg0_rwts = peg0_cases
                  |> ISPEC ``ltlPEG`` |> CONJUNCTS
                  |> map (fn th => map (fn t => Q.SPEC t th)
                                       [`tok P f`, `choice e1 e2 f`,
                                        `seq e1 e2 f`, `lws p`, `rpt p f`,
                                        `tokP t`, `empty l`])
                  |> List.concat
                  |> map (CONV_RULE
                            (RAND_CONV (SIMP_CONV (srw_ss())
                                                  [tokP_def, lws_def])))

fun peg0_nt t =
  let
    val th = Q.prove(
      ‘¬peg0 ltlPEG (lnt ^t)’,
      simp[lnt_def] >> simp[Once peg0_cases] >>
      simp(FDOM_ltlPEG :: ltlPEG_applied :: choicel_def :: token_def ::
           ident_def :: identchar_def :: peg0_rwts))
    val _ = augment_srw_ss [rewrites [th]]
  in
    th
  end

val topoNTs = [“nBase”, “nU”, “nConj”, “nDisj”, “nImp”, “nStart”]
val peg0_nts = save_thm("peg0_nts[simp]", LIST_CONJ (map peg0_nt topoNTs))

val wfpeg_lnt = wfpeg_cases
                  |> ISPEC ``ltlPEG``
                  |> Q.SPEC `lnt n`
                  |> CONV_RULE (RAND_CONV (SIMP_CONV (srw_ss()) [lnt_def]))

fun wfnt(t,acc) =
  let
    val th =
        Q.prove(`wfpeg ltlPEG (lnt ^t)`,
          SIMP_TAC (srw_ss())
                   [ltlPEG_applied, wfpeg_lnt, FDOM_ltlPEG,
                    choicel_def, token_def, lws_def, ident_def,
                    identchar_def] THEN
          simp(peg0_nts :: peg0_rwts @ wfpeg_rwts @ acc))
in
  th::acc
end;

val wfnts = List.foldl wfnt [] topoNTs

val subexprs_lnt = Q.prove(
  `subexprs (lnt n) = {lnt n}`,
  simp[subexprs_def, lnt_def]);

val frange_image = Q.prove(
  `FRANGE fm = IMAGE (FAPPLY fm) (FDOM fm)`,
  simp[finite_mapTheory.FRANGE_DEF, pred_setTheory.EXTENSION] >> metis_tac[]);

val peg_start = SIMP_CONV(srw_ss()) [ltlPEG_def]``ltlPEG.start``

val peg_range =
    SIMP_CONV (srw_ss())
              [FDOM_ltlPEG, frange_image, ltlPEG_applied]
              ``FRANGE ltlPEG.rules``
val PEG_exprs = save_thm(
  "PEG_exprs",
  ``Gexprs ltlPEG``
    |> SIMP_CONV (srw_ss())
         [Gexprs_def, subexprs_def, peg_range,
          subexprs_lnt, peg_start, choicel_def, token_def,
          pred_setTheory.INSERT_UNION_EQ
         ])

val PEG_wellformed = Q.store_thm(
  "PEG_wellformed",
  `wfG ltlPEG`,
  simp[wfG_def, Gexprs_def, subexprs_def,
       subexprs_lnt, peg_start, peg_range, DISJ_IMP_THM, FORALL_AND_THM,
       choicel_def, token_def, lws_def, tokP_def, ident_def, identchar_def] >>
  simp(peg0_nts :: wfpeg_rwts @ peg0_rwts @ wfnts));

(* test cases

EVAL ``parse "p -> q"``;
EVAL ``parse "p & q"``;
EVAL ``parse "p U q -> r"``;
EVAL ``parse "p U (q -> r)"``;
EVAL ``parse "Gp U (q -> r)"``;
EVAL ``parse "Xp U (q -> r)"``;
EVAL ``parse "Xpp U (q -> r)  "``;
*)


val _ = export_theory();
