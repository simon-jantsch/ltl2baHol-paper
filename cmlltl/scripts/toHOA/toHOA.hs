import Data.String
import Data.List

teststring =
  "(GBA (graph (spt (1 ()) (0 ((U (DISJ (N_VAR) (VAR)) (VAR a))))) (spt (1 (((gbE () () ((U (DISJ (N_VAR ) (VAR )) (VAR a)))) 1))) (0 (((gbE () () ()) 0) ((gbE () () ()) 0) ((gbE (a) () ((U (DISJ (N_VAR ) (VAR )) (VAR a)))) 1)))) (spt (1 ()) (0 ())) 2) (0) ((U (DISJ (N_VAR ) (VAR )) (VAR a))) (a))" :: String

teststring2_noproc = "(GBA (graph (spt (1 ()) (0 ((U (DISJ (N_VAR \"\") (VAR \"\")) (R (CONJ (VAR \"\") (N_VAR \"\")) (VAR \"a\"))))) (2 ((R (CONJ (VAR \"\") (N_VAR \"\")) (VAR \"a\"))))) (spt (1 (((gbE () () ((U (DISJ (N_VAR \"\") (VAR \"\")) (R (CONJ (VAR \"\") (N_VAR \"\")) (VAR \"a\"))))) 1))) (0 (((gbE (\"a\") () ((U (DISJ (N_VAR \"\") (VAR \"\")) (R (CONJ (VAR \"\") (N_VAR \"\")) (VAR \"a\"))))) 2) ((gbE () (\"\") ()) 0) ((gbE (\"\") () ()) 0) ((gbE (\"a\" \"\") (\"\") ((U (DISJ (N_VAR \"\") (VAR \"\")) (R (CONJ (VAR \"\") (N_VAR \"\")) (VAR \"a\"))))) 1))) (2 (((gbE (\"a\") () ((U (DISJ (N_VAR \"\") (VAR \"\")) (R (CONJ (VAR \"\") (N_VAR \"\")) (VAR \"a\"))))) 2) ((gbE (\"a\" \"\") (\"\") ((U (DISJ (N_VAR \"\") (VAR \"\")) (R (CONJ (VAR \"\") (N_VAR \"\")) (VAR \"a\"))))) 1)))) (spt (1 ()) (0 ()) (2 ())) 3) (0) ((U (DISJ (N_VAR \"\") (VAR \"\")) (R (CONJ (VAR \"\") (N_VAR \"\")) (VAR \"a\")))) (\"\" \"\" \"\" \"\" \"a\"))" :: String

teststring2 =
   "(GBA (graph (spt (1 ()) (0 ((U (DISJ (N_VAR ) (VAR )) (R (CONJ (VAR ) (N_VAR )) (VAR a))))) (2 ((R (CONJ (VAR ) (N_VAR )) (VAR a))))) (spt (1 (((gbE () () ((U (DISJ (N_VAR ) (VAR )) (R (CONJ (VAR ) (N_VAR )) (VAR a))))) 1))) (0 (((gbE (a) () ((U (DISJ (N_VAR ) (VAR )) (R (CONJ (VAR ) (N_VAR )) (VAR a))))) 2) ((gbE () () ()) 0) ((gbE () () ()) 0) ((gbE (a) () ((U (DISJ (N_VAR ) (VAR )) (R (CONJ (VAR ) (N_VAR )) (VAR a))))) 1))) (2 (((gbE (a) () ((U (DISJ (N_VAR ) (VAR )) (R (CONJ (VAR ) (N_VAR )) (VAR a))))) 2) ((gbE (a) () ((U (DISJ (N_VAR ) (VAR )) (R (CONJ (VAR ) (N_VAR )) (VAR a))))) 1)))) (spt (1 ()) (0 ()) (2 ())) 3) (0) ((U (DISJ (N_VAR ) (VAR )) (R (CONJ (VAR ) (N_VAR )) (VAR a)))) (a))" :: String

preproc :: String -> String
preproc [] = []
preproc ('\"' : '\"' : xs) = 'n':(preproc xs)
preproc ('"' : c : '"' : xs) = c:(preproc xs)
preproc (x:xs) = x:(preproc xs)

data Trans = Trans Int [(Char,Bool)] Int [Int]

print_trans :: (Char -> Int) -> Trans -> String
print_trans ap_fun (Trans from aps to acc_sets) =
  "[" ++ show_aps ap_fun aps ++ "] " ++ (show to)
  ++ show_acc_sets acc_sets ++ "\n"

show_acc_sets :: [Int] -> String
show_acc_sets [] = []
show_acc_sets xs = " { " ++ flatten (map (\as -> show as ++ " ") xs) ++ " }"

show_aps :: (Char -> Int) -> [(Char,Bool)] -> String
show_aps _ [] = "t"
show_aps ap_fun ((a,True):xs) =
  show (ap_fun a) ++ "&" ++ (show_aps ap_fun xs)
show_aps ap_fun ((a,False):xs) =
  "!" ++ show (ap_fun a) ++ "&" ++ (show_aps ap_fun xs)

data HOA = HOA Int [Int] [Char] [Trans] Int

print_hoa :: HOA -> String
print_hoa (HOA n init aps trans n_acc_sets) =
  let edges_grpd =
        groupBy
        (\e1 e2 -> (fst e1 == fst e2))
        (map (\t@(Trans s1 l s2 acc) -> (s1,t)) trans)
      edges = map ((\(s,trans) -> (head s,trans)) . unzip) edges_grpd
      start_block = flatten $
        map (\i -> "Start: " ++ (show i) ++  "\n") init
      ap_fun = \int a ->
        if int >= length aps
        then 99
        else if (aps!!int == a)
             then int
             else ap_fun (int+1) a
      edges_block = flatten $
        map
        (\(s,trns) ->
           "State: " ++ (show s) ++ "\n"
          ++ (flatten $ map (print_trans (ap_fun 0)) trns)
        )
        edges
      acc_string =
        show n_acc_sets
        ++ " (t"
        ++ flatten (map (\i -> " & Inf(" ++ show i ++ ")") [0..n_acc_sets-1])
        ++ ")"
      ap_string =
        foldl (\sf c -> sf ++ ['"',c,'"',' ']) [] (dup aps)
  in "HOA: v1\n"
     ++ "States: " ++ (show n) ++ "\n"
     ++ start_block
     ++ "AP: " ++ (show $ length aps) ++ " " ++ ap_string ++ "\n"
     ++ "Acceptance: " ++ acc_string ++ "\n"
     ++ "--BODY--\n"
     ++ edges_block
     ++ "--END--\n"



run :: [String] -> IO ()
run [] = return ()
run (x:xs) = (putStrLn x) >> (run xs)

-- Parses the top parenthesis structure of a string
parse_par :: String -> [String]
parse_par [] = []
parse_par ('(':ls) =
  let (par,rem) = collect_par ls 1
  in (par:(parse_par rem))
parse_par (c:ls) = parse_par ls

collect_par :: String -> Int -> (String,String)
collect_par [] _ = ([],[])
collect_par ('(':ls) i =
  let (par,res) = collect_par ls (i+1)
  in (('(':par),res)
collect_par (')':ls) 1 = ([],ls)
collect_par (')':ls) i =
  let (par,res) = collect_par ls (i-1)
  in ((')':par),res)
collect_par (c:ls) i =
  let (par,res) = collect_par ls i
  in ((c:par),res)

parseHOA :: String -> HOA
parseHOA str =
  let aut = parse_par (head (parse_par str))
      graph = head aut
      ap = dup (filter (\l -> l /= ' ') (aut!!3))
      graph_ps = parse_par graph
      nodes = head graph_ps
      nodes_count = length (parse_par nodes)
      edges = graph_ps!!1
      init = aut!!1
      acc_fun = \int frml ->
        if (int >= (length (aut!!2)))
        then 99
        else if ((parse_par (aut!!2))!!int == frml)
             then int
             else acc_fun (int+1) frml
  in (HOA
      nodes_count
      (map read (words init))
      ap
      (parse_edges edges acc_fun)
      (length (parse_par (aut!!2)))
     )

main :: IO ()
main =
  let waitForRightInput = do
        str <- getLine
        if (str == "Resulting GBA:")
          then return ()
          else waitForRightInput
  in do
    waitForRightInput
    inputString <- getLine
    run (lines (print_hoa (parseHOA (preproc inputString))))

parse_edges :: String -> (Int -> String -> Int) -> [Trans]
parse_edges edges acc_fun =
  let edges_per_state = parse_par edges
      grouped_edges =
        flatten $
        map
        (\es ->
           map (\str -> (read $ head (words es),str))
           (parse_par . head $ parse_par es)
        )
        edges_per_state
  --in show grouped_edges
  in map (parse_edge acc_fun) grouped_edges

parse_edge :: (Int -> String -> Int) -> (Int,String) -> Trans
parse_edge acc_fun (from_state,edge) =
  let edge_ps = parse_par . head $ parse_par edge
      to_state = read $ last (words edge)
      pos_ap = map (\ap -> (ap,True)) (filter (\l -> l /= ' ') (edge_ps!!0))
      neg_ap = map (\ap -> (ap,False)) (filter (\l -> l /= ' ') (edge_ps!!1))
      acc_sets = parse_par (edge_ps!!2)
  in Trans from_state (pos_ap ++ neg_ap) to_state (map (acc_fun 0) acc_sets)

flatten :: [[a]] -> [a]
flatten [] = []
flatten (x:xs) = x++(flatten xs)

dup :: Eq a => [a] -> [a]
dup [] = []
dup (x:xs) = x:(filter (\c -> c /= x) (dup xs))

