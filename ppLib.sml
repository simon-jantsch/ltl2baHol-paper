structure ppLib =
struct

local open HolKernel Parse boolLib
in

  val _ = temp_disable_tyabbrev_printing "alist"

  val _ = Feedback.set_trace "PP.print_firstcasebar" 1

  (* delete line below to have sets print as .. -> bool again *)
  val _ = temp_type_abbrev ("set", “:'a -> bool”)


val _ = temp_remove_termtok { tok = "=", term_name = "="}
val _ = temp_add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infix(NONASSOC, 450),
                  term_name = "=",
                  pp_elements = [HardSpace 1, TOK "=", BreakSpace(1,4),
                                 BeginFinalBlock(PP.CONSISTENT, 2)]}

val _ = temp_remove_termtok { tok = "==>", term_name = "⇒"}
val _ = temp_add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infixr 200,
                  term_name = "==>",
                  pp_elements = [HardSpace 1, TOK "⇒", BreakSpace(1,4)]}

val _ = temp_add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                       paren_style = OnlyIfNecessary,
                       fixity = Suffix 2100,
                       term_name = "suff",
                       pp_elements = [TOK "(suffL)", TM, TOK "(suffR)"]}

val _ = temp_overload_on
          ("ltlvwaaA", ``λphi. ltl2vwaa_free_alph (POW (props phi))``)

val _ = temp_add_rule {fixity = Prefix 2200,
                       block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                       paren_style = OnlyIfNecessary,
                       term_name = "ltlvwaaA",
                       pp_elements = [TOK "(ltlvwaaAL)", TM, TOK "(ltlvwaaAR)",
                                      HardSpace 1]}

fun powpp (tyG,tmG) bend sysp ppfns (lg,rg,topg) dpth t =
  let
    open term_pp_types smpp
    val {add_string,ublock,add_break,...} = ppfns
    fun paren b p = if b then
                      ublock PP.INCONSISTENT 1
                             (add_string "(" >> p >> add_string ")")
                    else p
  in
    paren (case lg of Prec(n,_) => n >= 2000 | _ => false) (
      add_string "POW" >>
      ublock PP.INCONSISTENT 1 (
        add_string "(" >>
        sysp {gravs=(Top,Top,Top), binderp=false, depth=dpth - 1} (rand t) >>
        add_string ")"
      )
    )
  end

val _ = temp_add_user_printer ("ppLib.POW_printer", ``POW s``, powpp)

end (* local *)

end (* struct *)
